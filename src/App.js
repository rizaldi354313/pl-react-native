import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Login from "./modules/Login";
const App = () => {
   return (
      <SafeAreaView style={styles.root}>
      <Login/>
      </SafeAreaView>
   );
};
const styles = StyleSheet.create({
   root: {
      flex:1,
      backgroundColor: '#F9FBFC'
   }
});
export default App;