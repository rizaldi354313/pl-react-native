import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, useWindowDimensions, View } from 'react-native';
import Logo from '../../../assets/images/logo_rb_landscape.png';
import CustomButton from '../../components/CustomButton';
import CustomInput from '../../components/CustomInput';
const Login = () => {
   const url = 'http://localhost/tibackend/v1/auth/OAuth/login';
   const [username, setUsername] = useState('');
   const [password, setPassword] = useState('');

   useEffect(async () => {
   }, []);
   // untuk dev
   const onSignInPressed = () => {
      console.warn(username);
      try {
         fetch('http://192.168.137.1/tibackend/v1/auth/OAuth/login',{
            method:'POST',
            headers: {
               'Content-Type': 'application/json'
            },
            body: JSON.stringify({
               username:username,
               password:password
            })
         })
         .then(response => response.json())
         .then(json => {
            console.warn(json.data.user_id);
            try {
            } catch (error) {
               console.warn(error);
            }
         });
      } catch (error) {
         alert(error.message);
      }
   } 
   const {height} = useWindowDimensions();
   return (
      <View style={styles.root}>
         <Image source={Logo} style={[styles.logo, {height: height * 0.3}]} resizeMode='contain'/>
         <CustomInput placeholder="Username" value={username} setValue={setUsername} />
         <CustomInput placeholder="Password" value={password} setValue={setPassword} secureTextEntry={true} />
         <CustomButton text="Login" onPress={onSignInPressed}/>
      </View>
   ); 
};     

const styles = StyleSheet.create({
   root : {
      alignItems : 'center',
      padding:20
   },
   logo: {
      width:'70%',
      maxWidth: 300,
      maxHeight: 200
   }
}); 
export default Login;
